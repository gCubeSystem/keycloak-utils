# Kycloak utils

**Some utilities functions to interact with Keycloak server

## Structure of the project

The source code is present in several folders dedidactaed to a specific language (e.g. python, xquery).

## Built With

N.A.

## Documentation

## Change log

See [CHANGELOG.md](CHANGELOG.md).

## Authors

* **Mauro Mugnaini** ([Nubisware S.r.l.](http://www.nubisware.com))

## How to Cite this Software
[Intentionally left blank]

## License

This project is licensed under the EUPL V.1.1 License - see the [LICENSE.md](LICENSE.md) file for details.

## About the gCube Framework
This software is part of the [gCubeFramework](https://www.gcube-system.org/ "gCubeFramework"): an
open-source software toolkit used for building and operating Hybrid Data
Infrastructures enabling the dynamic deployment of Virtual Research Environments
by favouring the realisation of reuse oriented policies.
 
The projects leading to this software have received funding from a series of European Union programmes see [FUNDING.md](FUNDING.md)

## Acknowledgments
[Intentionally left blank]